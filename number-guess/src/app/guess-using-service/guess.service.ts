import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { Win } from './win';


const API_BASE_URL = 'http://localhost:8080/number-guess-service/app/service';



@Injectable()
export class GuessService{

    constructor(private http:HttpClient){

    }

    getAverage() : Observable<number>{
        return this.http.get<number>(`${API_BASE_URL}/average`);
    }

    postWin(data : Win):Observable<number>{
        return this.http.post<number>(`${API_BASE_URL}`,data);
    }

}