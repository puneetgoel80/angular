import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuessUsingServiceComponent } from './guess-using-service.component';

describe('GuessUsingServiceComponent', () => {
  let component: GuessUsingServiceComponent;
  let fixture: ComponentFixture<GuessUsingServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuessUsingServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuessUsingServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
