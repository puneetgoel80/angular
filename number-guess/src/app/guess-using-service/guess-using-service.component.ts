import { Component, OnInit } from '@angular/core';
import { GuessService } from './guess.service';
import { Win } from './win';

@Component({
  selector: 'app-guess-using-service',
  templateUrl: './guess-using-service.component.html',
  styleUrls: ['./guess-using-service.component.css']
})
export class GuessUsingServiceComponent implements OnInit {
 
 
  private deviation : number;
  private guesses : number;
  private original : number;
  private guess : number;
  private showLow : boolean;
  private showHigh : boolean;
  private showWin : boolean;
  private message : string;
  private averageWin : number;

  



  
  constructor(private guessService:GuessService) { }

  ngOnInit() {
    this.guesses = 0;
    this.original = Math.floor((Math.random() * 1000)+1);
    console.log(this.original);
    this.guess= null;
    this.deviation = null;
    this.showLow = false;
    this.showHigh = false;
    this.showWin = false;
    this.message = "";
    this.guessService.getAverage().subscribe(data=>this.averageWin = data);
  }

verifyGuess(){
  this.deviation = this.original - this.guess;
  this.guesses = this.guesses + 1;
  if(this.deviation > 0){
    this.showLow = true;
    this.showHigh = false;
    this.showWin = false;
  }else if(this.deviation < 0 ){
    this.showLow = false;
    this.showHigh = true;
    this.showWin = false;
  }else {
    this.guessService.postWin(new Win(this.guesses))
    .subscribe(
      data=> console.log("logging data "+data),
      err=> this.message = err,
    ()=> this.message = "Your win is registered on server");

    this.showLow = false;
    this.showHigh = false;
    this.showWin = true;
  }
}



}
