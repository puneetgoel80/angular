import { Component, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-guess-container',
  templateUrl: './guess-container.component.html',
  styleUrls: ['./guess-container.component.css']
})
export class GuessContainerComponent implements OnInit {

 
 private deviation : number;
  
 private original : number;
 private guess : number;
 private showLow : boolean;
 private showHigh : boolean;
 private showWin : boolean;
 private guesses : number;


  
  constructor() { }

  ngOnInit() {
    this.guesses = 0;
    this.original = Math.floor((Math.random() * 1000)+1);
    console.log(this.original);
    this.guess= null;
    this.deviation = null;
    this.showLow = false;
    this.showHigh = false;
    this.showWin = false;
  }

verifyGuess(){
  this.deviation = this.original - this.guess;
  this.guesses = this.guesses + 1;

  this.showLow = false;
  this.showHigh = false;
  this.showWin = false;

  if(this.deviation > 0){
    this.showLow = true;
 
  }else if(this.deviation < 0 ){
      this.showHigh = true;
  
  }else {
      this.showWin = true;
  }
}
}
