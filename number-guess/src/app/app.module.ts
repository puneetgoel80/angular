import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';


import { AppComponent } from './app.component';
import { GuessOriginalComponent } from './guess-original/guess-original.component';
import { GuessRefactoredComponent } from './guess-refactored/guess-refactored.component';
import { GuessContainerComponent } from './guess-container/guess-container.component';
import { GuessPresentationComponent } from './guess-presentation/guess-presentation.component';
import { GuessUsingServiceComponent } from './guess-using-service/guess-using-service.component';
import { GuessService } from './guess-using-service/guess.service';

@NgModule({
  declarations: [
    AppComponent,
    GuessOriginalComponent,
    GuessRefactoredComponent,
    GuessContainerComponent,
    GuessPresentationComponent,
    GuessUsingServiceComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule

  ],
  providers: [GuessService],
  bootstrap: [AppComponent]
})
export class AppModule { }
