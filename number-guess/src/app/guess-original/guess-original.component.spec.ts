import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuessOriginalComponent } from './guess-original.component';

describe('GuessOriginalComponent', () => {
  let component: GuessOriginalComponent;
  let fixture: ComponentFixture<GuessOriginalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuessOriginalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuessOriginalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
