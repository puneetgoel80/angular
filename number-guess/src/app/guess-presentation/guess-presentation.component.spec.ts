import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuessPresentationComponent } from './guess-presentation.component';

describe('GuessPresentationComponent', () => {
  let component: GuessPresentationComponent;
  let fixture: ComponentFixture<GuessPresentationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuessPresentationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuessPresentationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
