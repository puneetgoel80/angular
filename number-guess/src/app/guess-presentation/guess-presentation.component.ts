import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-guess-presentation',
  templateUrl: './guess-presentation.component.html',
  styleUrls: ['./guess-presentation.component.css']
})
export class GuessPresentationComponent implements OnInit {

 
  @Input() private guesses : number;
  @Input() private showLow : boolean;
  @Input() private showHigh : boolean;
  @Input() private showWin : boolean;



  
  constructor() { }

  ngOnInit() {
    
  }

}
