import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuessRefactoredComponent } from './guess-refactored.component';

describe('GuessRefactoredComponent', () => {
  let component: GuessRefactoredComponent;
  let fixture: ComponentFixture<GuessRefactoredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuessRefactoredComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuessRefactoredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
