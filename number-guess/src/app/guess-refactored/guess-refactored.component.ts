import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-guess-refactored',
  templateUrl: './guess-refactored.component.html',
  styleUrls: ['./guess-refactored.component.css']
})
export class GuessRefactoredComponent implements OnInit {

  private deviation : number;
  private guesses : number;
  private original : number;
  private guess : number;
  private showLow : boolean;
  private showHigh : boolean;
  private showWin : boolean;



  
  constructor() { }

  ngOnInit() {
    this.guesses = 0;
    this.original = Math.floor((Math.random() * 1000)+1);
    console.log(this.original);
    this.guess= null;
    this.deviation = null;
    this.showLow = false;
    this.showHigh = false;
    this.showWin = false;
  }

verifyGuess(){
  this.deviation = this.original - this.guess;
  this.guesses = this.guesses + 1;
  if(this.deviation > 0){
    this.showLow = true;
    this.showHigh = false;
    this.showWin = false;
  }else if(this.deviation < 0 ){
    this.showLow = false;
    this.showHigh = true;
    this.showWin = false;
  }else {
    this.showLow = false;
    this.showHigh = false;
    this.showWin = true;
  }
}
}
