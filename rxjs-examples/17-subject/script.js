const { Subject } = rxjs;
// const { catchError, retry } = rxjs.operators;

function print(val) {
    let el = document.createElement('p');
    el.innerText = val;
    document.body.appendChild(el);
}

// Acts like a normal observable.
// Effectively broadcasts data to subscribers without relying
// on source data.
// "An observer that's also able to emit values"

// 1. Create the subject
const subject = new Subject();

// 2. Subscribe, in order to create an observer
const subscriberA = subject.subscribe(val => print(`Subscriber A: ${val}`));
const subscriberB = subject.subscribe(val => print(`Subscriber B: ${val}`));

// 3. Call next on the subscriber to start emitting values
subject.next('Hello');

// Another next.
setTimeout(() => {
    subject.next('World');
}, 1000);