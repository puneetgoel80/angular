const { fromEvent } = rxjs;

function print(val) {
    let el = document.createElement('p');
    el.innerText = val;
    document.body.appendChild(el);
}

// Don't need to create an Observable explicitly ...
const click$ = fromEvent(document, 'click')

click$.subscribe(click$ => console.log(click$))
