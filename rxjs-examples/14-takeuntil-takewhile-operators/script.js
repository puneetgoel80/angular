const { interval, timer, of } = rxjs;
const { takeUntil, finalize, takeWhile } = rxjs.operators;

function print(val) {
    let el = document.createElement('p');
    el.innerText = val;
    document.body.appendChild(el);
}

// 01. Using takeUntil()
// interval emits until notifier notifies.
// Effectively unsubscribing without calling unsubscribe.
const intervaltick$ = interval(500)
const notifier$ = timer(3000)

intervaltick$.pipe(
    takeUntil(notifier$),
    finalize(() => print('Complete!'))
).subscribe(num => print(num));

// 02. Using takeWhile()
// Related; unsubscribes when a value is found.
const namestakewhile = of('Bob', 'Jeff', 'Doug', 'Steve');

namestakewhile.pipe(
    takeWhile(name => name != 'Doug'),
    finalize(() => print('Complete! I found Doug'))
).subscribe(name => print(name));