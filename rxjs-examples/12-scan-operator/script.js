const { fromEvent } = rxjs;
const { map, tap, scan } = rxjs.operators;

function print(val) {
    let el = document.createElement('p');
    el.innerText = val;
    document.body.appendChild(el);
}

let gameClicks$ = fromEvent(document, 'click');

gameClicks$.pipe(
    // Each click get a random score.
    map(e => parseInt(Math.random() * 10)),
    // Just print to the screen.
    tap(score => print(`Click scored + ${score}`)),
    // Keeping a running total. Like array.reduce
    scan((highScore, score) => highScore + score))
.subscribe(highScore => print(`High Score ${highScore}`));