const { fromEvent, interval } = rxjs;
const { switchMap } = rxjs.operators;

function print(val) {
    let el = document.createElement('p');
    el.innerText = val;
    document.body.appendChild(el);
}

// Getting a 2nd Observable from a 1st Observable.
const intervalClicks$ = fromEvent(document, 'click');

// Not interested in the click, rather in the interval.
intervalClicks$.pipe(
    switchMap(click => {
        return interval(500)
    })).subscribe(i => print(i));

// Common use would be get an ID before undetaking a query.