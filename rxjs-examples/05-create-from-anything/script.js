const { of } = rxjs;

function print(val) {
    let el = document.createElement('p');
    el.innerText = val;
    document.body.appendChild(el);
}

const mashup = of('anything at all', ['for', 'real'], 
                            45, true, {nice: 'work'} )

mashup.subscribe(val => print(val))