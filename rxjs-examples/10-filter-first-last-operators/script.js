const { of } = rxjs;
const { filter, first, last } = rxjs.operators;

function print(val) {
    let el = document.createElement('p');
    el.innerText = val;
    document.body.appendChild(el);
}

const numbersv2 = of(-3, 5, 7, 2, -7, 9, -2);

numbersv2.pipe(
    filter(n => n >= 0)).subscribe(n => print(`Filtered ${n}`));

numbersv2.pipe(first()).subscribe(n => print(`First ${n}`));

numbersv2.pipe(last()).subscribe(n => print(`Last ${n}`));