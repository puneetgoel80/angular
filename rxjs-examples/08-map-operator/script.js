const { of } = rxjs;
const { map } = rxjs.operators;

function print(val) {
    let el = document.createElement('p');
    el.innerText = val;
    document.body.appendChild(el);
}

// 01. Map results to different outcomes.
const numbers = of(10, 100, 1000)

numbers.pipe(
    map(num => Math.log(num))
).subscribe(x => print(x));

// 02. More practically, calling an API that returns JSON
// and converting to JS.
const jsonString = '{ "type": "Dog", "breed": "Pug" }';
const apiCall = of(jsonString);

apiCall.pipe(map(json => JSON.parse(json)))
.subscribe(obj => {
    print(obj.type);
    print(obj.breed);
})