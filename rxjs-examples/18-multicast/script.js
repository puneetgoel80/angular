const { Subject, fromEvent } = rxjs;
const { tap, multicast } = rxjs.operators;

function print(val) {
    let el = document.createElement('p');
    el.innerText = val;
    document.body.appendChild(el);
}

// Send values to multiple subscribers.
const observableforMulticast = fromEvent(document, 'click');

const clicks = observableforMulticast.pipe(
    // Doing this is a side effect, but only runs once.
    tap(_ => print('Do One Time!')),
    multicast(() => new Subject())
);

const subAformulti = clicks.subscribe(c => print(`Sub A: ${c.timeStamp}`));
const subBformulti = clicks.subscribe(c => print(`Sub B: ${c.timeStamp}`));

clicks.connect();