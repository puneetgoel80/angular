const { fromEvent } = rxjs;
const { throttleTime, debounceTime } = rxjs.operators;

function print(val) {
    let el = document.createElement('p');
    el.innerText = val;
    document.body.appendChild(el);
}

const mouseEvents$ = fromEvent(document, 'mousemove');

// Only get first, then delay emitting new events.
mouseEvents$.pipe(throttleTime(1000))
    .subscribe(e => print(e.type));

// Wait 1s after last event. Good for autocomplete forms.
mouseEvents$.pipe(debounceTime(1000))
    .subscribe(e => print(e.type));