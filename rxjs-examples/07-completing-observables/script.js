const { timer, interval } = rxjs;
const { finalize } = rxjs.operators;

function print(val) {
    let el = document.createElement('p');
    el.innerText = val;
    document.body.appendChild(el);
}

// 01. Obs send a completed signal, finalize will run when completed.
const timer$ = timer(1000);

timer$.pipe(
    finalize(() => print('Complete!'))
).subscribe();

// 02. Some obs don't complete automatically, which can result
// in memory leaks.
const interval$ = interval(500).pipe(
    finalize(() => print('Complete!'))
);

const subscription = interval$.subscribe(x => 
    print('interval:' + x));

// Manually force observable to send complete signal after 3 seconds.
setTimeout(() => {
    subscription.unsubscribe()
}, 3000);