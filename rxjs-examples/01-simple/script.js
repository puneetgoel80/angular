const { Observable } = rxjs;

function print(val) {
    let el = document.createElement('p');
    el.innerText = val;
    document.body.appendChild(el);
}

// Create a simple Observable (an array that is built over time).
const observable$ = Observable.create(observer => {
    // Send a value.
    observer.next('Hello');
    observer.next('World');
    observer.next('Just learning some RxJS');
    // observer.error('Big error here');
    observer.complete();
})

// Call subscribe to start emitting.
observable$.subscribe(value => {
    print(value)
    },
    err => print('Error', err.message || err),
    () => print('Complete')
);