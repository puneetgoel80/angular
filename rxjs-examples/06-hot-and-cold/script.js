const { Observable } = rxjs;
const { publish, connect } = rxjs.operators;

function print(val) {
    let el = document.createElement('p');
    el.innerText = val;
    document.body.appendChild(el);
}

// 01. Cold observables. Data created within, not until subscription.
const cold$ = Observable.create(observer => {
    observer.next(Math.random())
});

// Number generated on subscription.
cold$.subscribe(a => print(`Subscriber A: ${a}`))
cold$.subscribe(b => print(`Subscriber B: ${b}`))

// 02. To make it hot, pass the value into the observable.
const randomNum = Math.random()

const hot$ = Observable.create(observer => {
    observer.next(randomNum)
});

// Number generated before subscription.
hot$.subscribe(c => print(`Subscriber C: ${c}`))
hot$.subscribe(d => print(`Subscriber D: ${d}`))

// 03. Alternatively, rather than decoupling the data from the observable,
// can use publish() and connect()
const alsoCold$ = Observable.create(observer => {
    observer.next(Math.random())
});

const hot2$ = alsoCold$.pipe(publish());

// Number generated before subscription.
hot2$.subscribe(e => print(`Subscriber E: ${e}`))
hot2$.subscribe(f => print(`Subscriber F: ${f}`))

hot2$.connect();