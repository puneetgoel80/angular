const { from } = rxjs;

function print(val) {
    let el = document.createElement('p');
    el.innerText = val;
    document.body.appendChild(el);
}

// Start with a Promise
const promise = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve('Complete!')
    }, 1000)
});

// Convert
const observablePromise = from(promise)

observablePromise.subscribe(result => print(result))
