const { of, zip, forkJoin } = rxjs;
const { delay } = rxjs.operators;

function print(val) {
    let el = document.createElement('p');
    el.innerText = val;
    document.body.appendChild(el);
}

// Using zip()
// Combing two observables into arrays based on index.
const yin$ = of('peanut butter', 'wine', 'rainbows');
const yang$ = of('jelly', 'cheese', 'unicorns');

const combo$ = zip(yin$, yang$);

combo$.subscribe(arr => print(arr));

// Using forkJoin()
// Wait for completion, then combine last 2 variables.
// Good for waiting until API calls are complete before sending
// data to the UI.
const yin2$ = of('peanut butter', 'wine', 'rainbows');
const yang2$ = of('jelly', 'cheese', 'unicorns').pipe(delay(2000));

const combo2$ = forkJoin(yin2$, yang2$);

combo2$.subscribe(arr => print(arr));