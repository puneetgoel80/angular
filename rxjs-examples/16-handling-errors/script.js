const { throwError } = rxjs;
const { catchError, retry } = rxjs.operators;

function print(val) {
    let el = document.createElement('p');
    el.innerText = val;
    document.body.appendChild(el);
}

const throwErrorObservable$ = throwError('catch me!');
throwErrorObservable$.pipe(
    catchError(err => print(`Error caught: ${err}`)),
    retry(2))
.subscribe(val => print(val), e => print(e));