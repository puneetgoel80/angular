const { Observable, merge } = rxjs;

function print(val) {
    let el = document.createElement('p');
    el.innerText = val;
    document.body.appendChild(el);
}

const observable = Observable.create((observer) => {
    observer.next('Hi')
})

const observable2 = Observable.create((observer) => {
    observer.next('Folks')
})

// Turn multiple observables into a single observable.
const newObservable = merge(observable, observable2);

newObservable.subscribe((x) => print(x));