const { of } = rxjs;
const { tap, map } = rxjs.operators;

function print(val) {
    let el = document.createElement('p');
    el.innerText = val;
    document.body.appendChild(el);
}

// Execute code without transforming the underlying data.
const names = of('Simon', 'Garfunkle');

names
    .pipe(
        tap(name => print(name)), 
        map(name => name.toUpperCase()),
        tap(name => print(name)))
    .subscribe();