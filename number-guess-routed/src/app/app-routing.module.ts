import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { GuessOriginalComponent } from './guess-original/guess-original.component';
import { GuessRefactoredComponent } from './guess-refactored/guess-refactored.component';
import { GuessUsingServiceComponent } from './guess-using-service/guess-using-service.component';

const appRoutes: Routes = [
  { path: 'original', component: GuessOriginalComponent },
  { path: 'refactored', component: GuessRefactoredComponent },
  { path: 'service', component: GuessUsingServiceComponent },
  { path: 'page-not-found', component: PageNotFoundComponent },
  { path: '**', redirectTo: '/page-not-found' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
