import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { GuessOriginalComponent } from './guess-original/guess-original.component';
import { GuessRefactoredComponent } from './guess-refactored/guess-refactored.component';
import { GuessUsingServiceComponent } from './guess-using-service/guess-using-service.component';
import { GuessService } from './guess.service';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    GuessOriginalComponent,
    GuessRefactoredComponent,
    GuessUsingServiceComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [GuessService],
  bootstrap: [AppComponent]
})
export class AppModule { }
