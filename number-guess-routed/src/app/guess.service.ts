import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
// import 'rxjs/add/operator/catch';
import { catchError } from 'rxjs/operators';
import { Win } from './win';

const API_URL = 'http://localhost:8080/number-guess-service/app/service';

@Injectable()
export class GuessService {

  constructor(private http: HttpClient) { }

  getAverageGuesses(): Observable<number> {
    return this.http.get<number>(`${API_URL}/average`);
  }

  // Moving to RxJS 6: can no longer return Observable<number>
  registerWin(win: Win): Observable<Object> {
    return this.http.post(API_URL, win).pipe(
      catchError((error: any) =>
        Observable.throw(error.json().error 
        || 'Server error'))
    );
  }
}