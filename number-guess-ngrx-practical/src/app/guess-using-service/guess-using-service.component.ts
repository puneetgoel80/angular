import { Component, OnInit } from '@angular/core';
import { GuessService } from '../guess.service';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { State, Reducer, Actions } from '../state';
import { Win } from '../models/win';

@Component({
  selector: 'guess-using-service',
  templateUrl: './guess-using-service.component.html',
  styleUrls: ['./guess-using-service.component.css']
})
export class GuessUsingServiceComponent implements OnInit {

  private deviation: number;
  private guesses: number;
  private original: number;
  private guess: number;
  private showHigh: boolean = true;
  private showLow: boolean = true;
  private showWin: boolean = true;
  private average: number;
  private error: string;
  private subscription: Subscription;
  private loading: boolean;

  constructor(
    private service: GuessService,
    private store: Store<State>
  ) {
    this.store.select(Reducer.getAverage).subscribe(avg => { 
      console.log(`Subscribe event for average fired: average = ${avg}`);
      this.average = avg;
    });
    this.store.select(Reducer.getLoading).subscribe(ldg => { 
      console.log(`Subscribe event for loading fired: loading = ${ldg}`);
      this.loading = ldg;
    });
  }

  ngOnInit() {
    this.guesses = 0;
    this.original =
      Math.floor((Math.random() * 1000) + 1);
    console.log(this.original);
    this.guess = null;
    this.deviation = null;
    this.showHigh = true;
    this.showLow = true;
    this.showWin = true;

    this.store.dispatch(new Actions.GetAverageGuesses);
  }

  verifyGuess() {

    this.deviation = this.original - this.guess;
    this.guesses = this.guesses + 1;

    this.showHigh = true;
    this.showLow = true;
    this.showWin = true;

    if (this.deviation < 0) {
      this.showHigh = false;
    } else if (this.deviation > 0) {
      this.showLow = false;
    } else {
      this.showWin = false;
      console.log("noOfTries: " + this.guesses);
      let winner = new Win(this.guesses);
      console.log(winner);
      this.store.dispatch(new Actions.RegisterWin(new Win(this.guesses)));

      // (Q) Dispatch RegisterWin action.

    }
  }

}
