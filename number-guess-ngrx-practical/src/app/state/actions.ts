import { Action } from '@ngrx/store';
import { Win } from '../models/win';

export const GET_AVERAGE_GUESSES = 'Get Average Guesses';
export const GUESSES_RETRIEVED = 'Guesses Retrieved';
export const REGISTER_WIN = 'Register win';

// (Q) Add REGISTER_WIN & REGISTER_WIN_COMPLETE constants.

export class GetAverageGuesses implements Action {
    readonly type = GET_AVERAGE_GUESSES;
}

export class GuessesRetrieved implements Action {
    readonly type = GUESSES_RETRIEVED;
    constructor(public payload: number) { }
}

export class RegisterWin implements Action {
    readonly type = REGISTER_WIN;
    constructor(public payload : Win){

    }
}

// (Q) Add RegisterWin Action

// (Q) Add RegisterWinComplete Action.

// (Q) Add new Actions to be exported.
export type All =
    | GetAverageGuesses
    | GuessesRetrieved
    |RegisterWin   ;