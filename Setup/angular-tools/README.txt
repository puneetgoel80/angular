------------------------
Installing Node.js & npm
------------------------

Install Node.js 8.11.3 using provided .msi

Upgrade npm:

	Run PowerShell as Administrator
	Set-ExecutionPolicy Unrestricted -Scope CurrentUser -Force
	npm install -g npm-windows-upgrade
	npm-windows-upgrade (choose 6.3.0)

Check: node -v (expect v8.11.2)
Check: npm -v (expect 6.3.0)

---------
npm Notes
---------

Since npm v5, --save is not necessary; installed modules are added as a dependency by default, under package.json/dependencies. 

--save-optional saves to package.json/devDependencies

--save-optional saves to package.json/optionalDependencies

--------------------
Updating Angular CLI
--------------------

npm uninstall -g angular-cli
npm cache verify
npm install -g @angular/cli@latest

Check: ng -v

-----------------
Installing Tomcat
-----------------

Unpack Tomcat to C:\apache-tomcat-8.5.23

Check JDK paths are correct in setenv-tomcat8.bat, and move to c:\setenv-tomcat8.bat

Open cmd and run .bat

Navigate to Tomcat bin directory and run startup.bat

Visit http://localhost:8080/manager/html (login admin/admin)

Below services are pre-deployed and run on startup.

--------
Services
--------

Forecast service:
GET - http://localhost:8080/forecast-service-api/sunday/afternoon

Number Guess service:
GET  - http://localhost:8080/number-guess-service/app/service/average
POST - http://localhost:8080/number-guess-service/app/service/ (Win payload)

Employee service:
GET/findAll  - http://localhost:8080/employee-api/employee/service
GET/findById - http://localhost:8080/employee-api/employee/service/{id}
POST/create  - http://localhost:8080/employee-api/employee/service (Employee payload)
POST/authenticate - http://localhost:8080/employee-api/employee/service/authenticate
PUT/update - http://localhost:8080/employee-api/employee/service (Employee payload)
DELETE/remove - http://localhost:8080/employee-api/employee/service/{id}
GET/error - http://localhost:8080/employee-api/employee/service/error

JWT service:
POST/authenticateUser - http://localhost:8080/sec-jwt-auth/authentication (String username, String password)
GET/users - http://localhost:8080/sec-jwt-auth/users (requires JWT token)