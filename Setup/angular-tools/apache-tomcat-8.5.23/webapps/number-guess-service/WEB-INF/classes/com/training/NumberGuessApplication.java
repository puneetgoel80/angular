package com.training;

import java.util.Set;
import java.util.HashSet;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/app")
public class NumberGuessApplication extends Application {
    @Override
    public Set<Class<?>> getClasses() {
    	Set<Class<?>> set = new HashSet<Class<?>>();
        set.add(NumberGuessService.class);
        return set;
    }
}