package com.training;

public interface NumberGuessDao {

	int registerWin(Win win);
	int getAverageGuesses();
}
