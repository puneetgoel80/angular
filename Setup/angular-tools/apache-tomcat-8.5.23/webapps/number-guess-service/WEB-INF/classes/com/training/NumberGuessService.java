package com.training;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/service")
public class NumberGuessService {

	static NumberGuessDao dao = new NumberGuessDaoImpl();
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Win test() {
		return new Win(20);
	}
	
	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response registerWin(Win win) {
		System.out.println("/register service : " + win);
		return Response.ok().entity(dao.registerWin(win)).build();
	}
	
	@GET
	@Path("/average")
	@Produces({ MediaType.TEXT_PLAIN })
	public int getAverageGuesses() {
		System.out.println("getAverageGuesses() called.");
		return dao.getAverageGuesses();
	}

}
