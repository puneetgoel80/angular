package com.training;

public class Forecast {

	String outlook;
	
	public Forecast() {
	}
	
	public Forecast(String outlook) {
		this.outlook = outlook;
	}
	
	public String getOutlook() {
		return outlook;
	}
	
	public void setOutlook(String outlook) {
		this.outlook = outlook;
	}
	
	@Override
	public String toString() {
		return outlook;
	}
}
