package com.training;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/afternoon")
public class ForecastService {

	ForecastDao dao = new ForecastDaoImpl();
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Forecast getForecast() {
		return dao.getForecast();
	}
	
	@GET
	@Path("/{from}/{to}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Forecast getForecast(@PathParam("from") int from, @PathParam("to") int to) {
		return dao.getForecast(from, to);
	}
}
