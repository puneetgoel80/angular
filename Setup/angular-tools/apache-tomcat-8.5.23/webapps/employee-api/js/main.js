var rootURL = "http://localhost:8080/rest-person-end2end-update/people/service";

$('#btnAdd').click(function() {
	newPerson();
	return false;
});

$('#btnSave').click(function() {
	if ($('#pId').val() == '')
		addPerson();
	else
		//updatePerson();
	return false;
});


function newPerson() {
	$('#btnDelete').hide();
	currentPerson = {};
	renderDetails(currentPerson); // Display empty form
}

function renderDetails(p) {
	$('#pId').val(p.id);
	$('#name').val(p.name);
	$('#url').val(p.url);
}


function addPerson() {
	console.log('[main.js] addPerson');
	$.ajax({
		type: 'POST',
		
		//headers: { 
	    //    'Accept': 'application/json',
	    //    'Content-Type': 'application/json' 
	    //},
		contentType: 'application/json',
		url: rootURL,
		//dataType: "json",
		data: formToJSON(),
		success: function(data, textStatus, jqXHR){
			console.log('Person created successfully: ' + textStatus);
			//$('#btnDelete').show();
			//$('#PersonId').val(data.id);
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('addPerson error: ' + textStatus);
		}
	});
}

//Helper function to serialize all the form fields into a JSON string
function formToJSON() {
	var pId = $('#pId').val();
	var jsonPerson = JSON.stringify({
		"id": pId == "" ? "0" : pId, 
		"name": $('#name').val(), 
		"url": $('#url').val()
		});
	console.log(jsonPerson);
	return jsonPerson;
	
	//return JSON.stringify({
	//	"id": personId == "" ? null : personId, 
	//	"gender": $('#gender').val(), 
	//	"password": $('#password').val(),
	//});
}

//var currentPerson;

// Retrieve person list when application starts 
findAll();
/*
// Nothing to delete in initial application state
$('#btnDelete').hide();
/*
// Register listeners
$('#btnSearch').click(function() {
	search($('#searchKey').val());
	return false;
});
*/
/*
// Trigger search when pressing 'Return' on search key input field
$('#searchKey').keypress(function(e){
	if(e.which == 13) {
		search($('#searchKey').val());
		e.preventDefault();
		return false;
    }
});
*/
/*
$('#btnAdd').click(function() {
	newPerson();
	return false;
});
*/
/*
$('#btnSave').click(function() {
	if ($('#personId').val() == '')
		addPerson();
	else
		updatePerson();
	return false;
});
*/
/*
$('#btnDelete').click(function() {
	deletePerson();
	return false;
});
*/

$('#personList').on('click', 'a', function() {
	console.log("findById should be called ...");
	findById($(this).data('identity'));
});

/*
// Replace broken images with generic wine bottle
$("img").error(function(){
  $(this).attr("src", "pics/generic.jpg");

});
*/

/*
function search(searchKey) {
	if (searchKey == '') 
		findAll();
	else
		findByName(searchKey);
}
*/

/*
function newPerson() {
	$('#btnDelete').hide();
	currentPerson = {};
	renderDetails(currentPerson); // Display empty form
}
*/

function findAll() {
	console.log('[main] findAll');
	$.ajax({
		type: 'GET',
		url: rootURL,
		dataType: "json", // data type of response
		success: renderList
	});
}

/*
function findByName(searchKey) {
	console.log('findByName: ' + searchKey);
	$.ajax({
		type: 'GET',
		url: rootURL + '/search/' + searchKey,
		dataType: "json",
		success: renderList 
	});
}
*/


function findById(id) {
	console.log('findById: ' + id);
	$.ajax({
		type: 'GET',
		url: rootURL + '/' + id,
		dataType: "json",
		success: function(data){
			$('#btnDelete').show();
			console.log('findById success: ' + data.name);
			currentPerson = data;
			renderDetails(currentPerson);
		}
	});
}

/*
function addPerson() {
	console.log('[main] addPerson');
	$.ajax({
		type: 'POST',
		contentType: 'application/json',
		url: rootURL,
		dataType: "json",
		data: formToJSON(),
		success: function(data, textStatus, jqXHR){
			console.dir('Person created successfully');
			$('#btnDelete').show();
			$('#PersonId').val(data.id);
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('addPerson error: ' + textStatus);
		}
	});
}
*/

/*
function updatePerson() {
	console.log('updatePerson');
	$.ajax({
		type: 'PUT',
		contentType: 'application/json',
		url: rootURL + '/' + $('#personId').val(),
		dataType: "json",
		data: formToJSON(),
		success: function(data, textStatus, jqXHR){
			alert('Person updated successfully');
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('updatePerson error: ' + textStatus);
		}
	});
}
*/
/*
function deletePerson() {
	console.log('[main] deletePerson');
	console.log("id: " + $('#personId').val());
	$.ajax({
		type: 'DELETE',
		url: rootURL + '/' + $('#personId').val(),
		//url: rootURL + '/' + prompt('id', 'id'),
		success: function(data, textStatus, jqXHR){
			alert('Person deleted successfully');
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('deletePerson error');
		}
	});
}
*/

// Used by findAll
function renderList(data) {

	// JAX-RS serializes an empty list as null, and a 'collection of one' as an object (not an 'array of one')
	var list = data == null ? [] : (data instanceof Array ? data : [data]);

	// Before using the web.xml directive for POJO's
	
//	for (var i = 0; i < data.person.length; i++) {
//    	//var counter = jsonData.counters[i];
//    	var person = data.person[i];
//		$('#personList').append('<li><a href="#" data-identity="' + person.id + '">' + person.name +'</a></li>');
//	}

	$('#personList li').remove();
	$.each(list, function(index, person) {
		$('#personList').append('<li><a href="#" data-identity="' + person.id + '">' + person.name +'</a></li>');
	});
}

/*
function renderDetails(person) {
	$('#personId').val(person.id);
	$('#name').val(person.name);
	$('#password').val(person.pasword);
}
*/

/*
// Helper function to serialize all the form fields into a JSON string
function formToJSON() {
	var personId = $('#personId').val();
	var jsonPerson = JSON.stringify({
		"id": personId == "" ? null : personId, 
		"gender": $('#gender').val(), 
		"password": $('#password').val()
		});
	return jsonPerson;
	
	//return JSON.stringify({
	//	"id": personId == "" ? null : personId, 
	//	"gender": $('#gender').val(), 
	//	"password": $('#password').val(),
	//});
	
}
*/