package com.training;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class EmployeeInMemoryDao implements EmployeeDao {

	private List<Employee> list;
	private AtomicInteger empId;

	public EmployeeInMemoryDao() {
		list = new CopyOnWriteArrayList<Employee>();
		empId = new AtomicInteger();
		list.add(new Employee());

		list.get(0).setId(empId.get());
		list.get(0).setSurname("Toast");
		list.get(0).setSalary(22222.22D);
		list.get(0).setUsername("toasty");
		list.get(0).setPassword("toastpass");

		list.add(new Employee());

		list.get(1).setId(empId.incrementAndGet());
		list.get(1).setSurname("Curtis");
		list.get(1).setSalary(33333.33D);
		list.get(1).setUsername("curty");
		list.get(1).setPassword("curtpass");

		list.add(new Employee());

		list.get(2).setId(empId.incrementAndGet());
		list.get(2).setSurname("Bob");
		list.get(2).setSalary(44444.44D);
		list.get(2).setUsername("boby");
		list.get(2).setPassword("bobpass");
	}

	public List<Employee> findAll() {
		return list;
	}

	public List<Employee> findByName(String name) {
		List<Employee> localList = new ArrayList<Employee>();
		for (Employee emp : list) {
			if (emp.getSurname().equals(name)) {
				localList.add(emp);
			}
		}
		return localList;
	}

	public Employee findById(int id) {
		Employee e1 = null;
		for (Employee emp : list) {
			if (emp.getId() == id) {
				e1 = list.get(emp.getId());
			}
		}
		return e1;
	}
	
	public Employee findByUsername(String username) {
    	Employee e1 = null;
    	for(Employee emp : list) {
    		if(emp.getUsername().equals(username)) {
    			e1 = list.get(emp.getId());
    		}
    	}
        return e1;
    }

	public Employee save(Employee emp) {
		return emp.getId() > 0 ? update(emp) : create(emp);
	}

	public Employee create(Employee emp) {
		Employee newEmp = new Employee();
		newEmp.setId(empId.incrementAndGet());
		newEmp.setSurname(emp.getSurname());
		newEmp.setSalary(emp.getSalary());
    	newEmp.setUsername(emp.getUsername());
    	newEmp.setPassword(emp.getPassword());
    	list.add(newEmp);
		System.out.println("Created employee: " + newEmp);
		return newEmp;
	}

	public Employee update(Employee emp) {
		Employee e = list.get(emp.getId());
		e.setId(emp.getId());
		e.setSurname(emp.getSurname());
		e.setSalary(emp.getSalary());
    	e.setUsername(emp.getUsername());
    	e.setPassword(emp.getPassword());
		return e;
	}

	public boolean remove(int id) {
		boolean removed = false;
		List<Employee> empList = findAll();
		for (Employee emp : empList) {
			if (emp.getId() == id) {
				int index = empList.indexOf(emp);
				empList.remove(index);
				list = empList;
				removed = true;
			}
		}
		return removed;
	}
}
