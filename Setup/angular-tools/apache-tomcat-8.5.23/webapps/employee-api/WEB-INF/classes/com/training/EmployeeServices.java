package com.training;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/service")
public class EmployeeServices {

	static EmployeeDao dao = new EmployeeInMemoryDao();
	private static final String SUCCESS = "SUCCESS";
	private static final String FAILURE = "FAILURE";

	@POST
	@Path("/authenticate")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Employee authenticate(Employee employee) {
		Employee e = dao.findByUsername(employee.getUsername());
		if (e != null && (e.getPassword().equals(employee.getPassword()))) {
			System.out.println("Authenticated.");
		} else {
			System.out.println("Rejected.");
			e = new Employee();
		}
		return e;
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Employee> findAll() {
		System.out.println("findAll: " + dao.findAll());
		return dao.findAll();
	}

	@GET
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findById(@PathParam("id") String id) {
		System.out.println("findById " + id + " " + dao.findById(Integer.parseInt(id)));
		return Response.ok() // 200
				.entity(dao.findById(Integer.parseInt(id))).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
	}

	// @GET
	// @Path("{surname}")
	// @Produces({ MediaType.APPLICATION_JSON })
	// public List<Employee> findBySurname(@PathParam("surname") String surname)
	// {
	// System.out.println("findBySurname " + surname);
	// return dao.findByName(surname);
	// }

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Employee create(Employee employee) {
		System.out.println("Creating employee.");
		return dao.create(employee);
	}

	@PUT
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Employee update(Employee employee) {
		System.out.println("Updating employee.");
		return dao.update(employee);
	}

	@DELETE
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String remove(@PathParam("id") int id) {
		System.out.println("Deleting employee with id: " + id);
		boolean result = dao.remove(id);
		if (result) {
			return SUCCESS;
		}
		return FAILURE;
	}

	@GET
	@Path("/error")
	public Response error() {
		System.out.println("error");
		return Response.status(500).build();
	}
}
