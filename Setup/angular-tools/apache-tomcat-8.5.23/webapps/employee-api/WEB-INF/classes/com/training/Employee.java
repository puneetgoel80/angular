package com.training;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="employee")
public class Employee implements Comparable<Employee> {

	private int id;    // identifier used as lookup-key
	private String surname;
	private double salary;
	private String username;
	private String password;
	
	public Employee() {}
	
	public Employee(String surname, double salary) {
		this.surname = surname;
		this.salary = salary;
	}
	
	@XmlElement
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	@XmlElement
	public String getSurname() {
		return surname;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	@XmlElement
	public double getSalary() {
		return salary;
	}
	
	public void setSalary(double salary) {
		this.salary = salary;
	}

	// Implementation of Comparable.
	public int compareTo(Employee other) {
		return this.id - other.id;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", surname=" + surname + ", username="
				+ username + ", password=" + password + "]";
	}
}