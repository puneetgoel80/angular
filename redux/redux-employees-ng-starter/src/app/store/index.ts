import { combineReducers } from 'redux';
import { EmployeesReducer } from './employees.reducer';
import { Employee } from '../model/employees';

export class IAppState {
  employees: Employee[];
};

export const rootReducer = combineReducers<IAppState>({
  employees: EmployeesReducer,
});


