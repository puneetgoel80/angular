import { Injectable } from '@angular/core';
import { IAppState } from '../store/index';
import { NgRedux } from '@angular-redux/store';

@Injectable()
export class EmployeesActions {

    static EMPLOYEES_GET = 'EMPLOYEES_GET';
    static EMPLOYEES_DELETE = 'EMPLOYEES_DELETE';

    constructor(
        private ngRedux: NgRedux<IAppState>,
      ) { }
      
      getEmployees() {
        this.ngRedux.dispatch({
          type: EmployeesActions.EMPLOYEES_GET,
        });
      }
    
      deleteEmployee(id): void {
        this.ngRedux.dispatch({
          type: EmployeesActions.EMPLOYEES_DELETE,
          payload: { id }
        });
      }
}
