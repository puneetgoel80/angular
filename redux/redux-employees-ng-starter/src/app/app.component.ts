import { Component, ViewChild } from '@angular/core';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs';

import { EmployeesActions } from './actions/employees.actions';
import { Employee } from './model/employees';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  @select('employees') public employees$: Observable<Employee>;

  constructor(public actions:  EmployeesActions) {
    actions.getEmployees();
  }

}
