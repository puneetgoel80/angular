import { InjectionToken } from '@angular/core';
import {
  createStore,
  //Store,
  compose,
  StoreEnhancer
} from 'redux';
// Commented out Store above, and use this instead.
import * as Redux from 'redux';

import { AppState } from './app.state';
import {
  counterReducer as reducer
} from './counter.reducer';

export const AppStore = new InjectionToken('App.store');

const devtools: StoreEnhancer<AppState> =
  window['devToolsExtension'] ?
  window['devToolsExtension']() : f => f;

export function createAppStore(): Redux.Store<AppState> {
  // createStore signature updated in 4.0.0
  // https://github.com/Microsoft/TypeScript-React-Starter/issues/136
  return createStore<AppState, any, any, any>(
    reducer,
    compose(devtools)
  );
}

export const appStoreProviders = [
   { provide: AppStore, useFactory: createAppStore }
];
