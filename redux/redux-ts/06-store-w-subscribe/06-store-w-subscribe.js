class Store {
    constructor(reducer, initialState) {
        this.reducer = reducer;
        this._listeners = [];
        this._state = initialState;
    }
    getState() {
        return this._state;
    }
    dispatch(action) {
        this._state = this.reducer(this._state, action);
        this._listeners.forEach((listener) => listener());
    }
    subscribe(listener) {
        this._listeners.push(listener);
        return () => {
            this._listeners = this._listeners.filter(l => l !== listener);
        };
    }
}
let reducer = (state, action) => {
    switch (action.type) {
        case 'INCREMENT':
            return state + 1;
        case 'DECREMENT':
            return state - 1;
        case 'PLUS':
            return state + action.payload;
        default:
            return state;
    }
};
let store = new Store(reducer, 0);
console.log(store.getState());
let unsubscribe = store.subscribe(() => {
    console.log('subscribed: ', store.getState());
});
store.dispatch({ type: 'INCREMENT' });
store.dispatch({ type: 'INCREMENT' });
unsubscribe();
store.dispatch({ type: 'DECREMENT' });
console.log(store.getState());
//# sourceMappingURL=06-store-w-subscribe.js.map