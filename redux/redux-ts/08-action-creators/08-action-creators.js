"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const miniRedux_1 = require("../lib/miniRedux");
class MessageActions {
    static addMessage(message) {
        return {
            type: 'ADD_MESSAGE',
            message: message
        };
    }
    static deleteMessage(index) {
        return {
            type: 'DELETE_MESSAGE',
            index: index
        };
    }
}
let reducer = (state, action) => {
    switch (action.type) {
        case 'ADD_MESSAGE':
            return {
                messages: state.messages.concat(action.message),
            };
        case 'DELETE_MESSAGE':
            let idx = action.index;
            return {
                messages: [
                    ...state.messages.slice(0, idx),
                    ...state.messages.slice(idx + 1, state.messages.length)
                ]
            };
        default:
            return state;
    }
};
let store = new miniRedux_1.Store(reducer, { messages: [] });
console.log(store.getState());
store.dispatch(MessageActions.addMessage('Would you say the fringe was made of silk?'));
store.dispatch(MessageActions.addMessage('Wouldnt have no other kind but silk'));
store.dispatch(MessageActions.addMessage('Has it really got a team of snow white horses?'));
console.log(store.getState());
store.dispatch(MessageActions.deleteMessage(1));
console.log(store.getState());
store.dispatch(MessageActions.deleteMessage(0));
console.log(store.getState());
//# sourceMappingURL=08-action-creators.js.map