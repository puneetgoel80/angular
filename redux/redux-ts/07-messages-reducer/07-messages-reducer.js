"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const miniRedux_1 = require("../lib/miniRedux");
let reducer = (state, action) => {
    switch (action.type) {
        case 'ADD_MESSAGE':
            return {
                messages: state.messages.concat(action.message),
            };
        case 'DELETE_MESSAGE':
            let idx = action.index;
            return {
                messages: [
                    ...state.messages.slice(0, idx),
                    ...state.messages.slice(idx + 1, state.messages.length)
                ]
            };
        default:
            return state;
    }
};
let store = new miniRedux_1.Store(reducer, { messages: [] });
console.log(store.getState());
store.dispatch({
    type: 'ADD_MESSAGE',
    message: 'Would you say the fringe was made of silk?'
});
store.dispatch({
    type: 'ADD_MESSAGE',
    message: 'Wouldnt have no other kind but silk'
});
store.dispatch({
    type: 'ADD_MESSAGE',
    message: 'Has it really got a team of snow white horses?'
});
console.log(store.getState());
store.dispatch({
    type: 'DELETE_MESSAGE',
    index: 1
});
console.log(store.getState());
store.dispatch({
    type: 'DELETE_MESSAGE',
    index: 0
});
console.log(store.getState());
//# sourceMappingURL=07-messages-reducer.js.map