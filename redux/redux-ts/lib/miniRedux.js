"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Store {
    constructor(reducer, initialState) {
        this.reducer = reducer;
        this._listeners = [];
        this._state = initialState;
    }
    getState() {
        return this._state;
    }
    dispatch(action) {
        this._state = this.reducer(this._state, action);
        this._listeners.forEach((listener) => listener());
    }
    subscribe(listener) {
        this._listeners.push(listener);
        return () => {
            this._listeners = this._listeners.filter(l => l !== listener);
        };
    }
}
exports.Store = Store;
//# sourceMappingURL=miniRedux.js.map