import { EmployeesActions } from '../actions/employees.actions';
import { Employees } from '../model/employees';

const INITIAL_STATE: Employees = {
  list: [],
  errors: []
};

export function EmployeesReducer(state: Employees = INITIAL_STATE, action: any): any {
  let index, errors, list;

  switch (action.type) {
    case EmployeesActions.EMPLOYEES_GET:
      return state;

    case EmployeesActions.EMPLOYEES_GET_SUCCESS:
      return Object.assign({}, state, { list: action.payload });

    case EmployeesActions.EMPLOYEES_GET_ERROR:
      return Object.assign({}, state, { errors: action.payload });;

    case EmployeesActions.EMPLOYEES_GET_CANCELLED:
      console.log('reducer:cancel')
      return state;

    default:
      return state;
  }
}

