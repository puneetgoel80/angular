import { Injectable } from '@angular/core';
import { IAppState } from '../store';
import { NgRedux } from '@angular-redux/store';

@Injectable()
export class EmployeesActions {
    static EMPLOYEES_GET = 'EMPLOYEES_GET';
    static EMPLOYEES_GET_SUCCESS = 'EMPLOYEES_GET_SUCCESS';
    static EMPLOYEES_GET_ERROR = 'EMPLOYEES_GET_ERROR';
    static EMPLOYEES_GET_CANCELLED = 'EMPLOYEES_GET_CANCELLED';

    constructor(private ngRedux: NgRedux<IAppState>) {}

    getEmployees() {
        this.ngRedux.dispatch({
            type: EmployeesActions.EMPLOYEES_GET,
            payload: null
        });
    }

    cancelEmployeeGet() {
        this.ngRedux.dispatch({
            type: EmployeesActions.EMPLOYEES_GET_CANCELLED
        });
    }
}
