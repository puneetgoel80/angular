import { InjectionToken } from '@angular/core';
import {
  createStore,
  //Store,
  compose,
  StoreEnhancer
} from 'redux';
import * as Redux from 'redux';

import { AppState } from './app.state';
import {
  employeeReducer as reducer
} from './employee.reducer';

export const AppStore = new InjectionToken('App.store');

const devtools: StoreEnhancer<AppState> =
  window['devToolsExtension'] ?
  window['devToolsExtension']() : f => f;

export function createAppStore(): Redux.Store<AppState> {
  return createStore<AppState, any, any, any>(
    reducer,
    compose(devtools)
  );
}

export const appStoreProviders = [
   { provide: AppStore, useFactory: createAppStore }
];
