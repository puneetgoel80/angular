import { Component, Inject } from '@angular/core';
//import { Store } from 'redux';
// Comment above and use Redux.Store instead.
import * as Redux from 'redux';
import { AppStore } from './app.store';
import { AppState } from './app.state';
// import * as EmployeeAddAction from './employee.actions';
// (Q) Import Employee delete.
import { EmployeeAddAction } from './employee.actions';
import { Employee } from './employee';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  employees: Employee [];

  constructor(@Inject(AppStore) private store: Redux.Store<AppState>) {
    store.subscribe(() => this.readState());
    this.readState();
  }

  getId = () => {
    return Math.floor(Math.random() * Math.floor(999999));
  }

  readState() {
    const state: AppState = this.store.getState() as AppState;
    this.employees = state.employeeList;
  }

  addEmployee() {
    this.store.dispatch({type: 'EMPLOYEE_ADD', payload: { "id": this.getId(),"surname": "Bazza", salary: 2323 }});
  }

  // (Q) Add deleteEmployee function to dispath EMPLOYEE_DELETE action

}
