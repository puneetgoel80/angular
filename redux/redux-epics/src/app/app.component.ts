import { Component } from '@angular/core';

import { NgRedux, select } from '@angular-redux/store';
import { createEpicMiddleware } from 'redux-observable';

import { Observable } from 'rxjs';
import { delay, mapTo } from 'rxjs/operators';

/// NOTE: files inlined for simplicity.

// store.ts 
export interface AppState {
  isPinging: boolean;
}

export const INITIAL_STATE: AppState = {
  isPinging: false,
};

// epic.ts
const pingEpic = action$ =>
  action$.ofType(PING).pipe(
    delay(1000), // Asynchronously wait 1000ms then continue
    mapTo({ type: PONG })
  );

// reducer.ts
const pingReducer = (state = { isPinging: false }, action) => {
  switch (action.type) {
    case PING:
      return { isPinging: true };

    case PONG:
      return { isPinging: false };

    default:
      return state;
  }
};

// actions.ts
const PING = 'PING';
const PONG = 'PONG';

const ping = () => ({ type: PING });

// app.component.ts
const epicMiddleware = createEpicMiddleware();
const middlewares = [epicMiddleware];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // Grab a "slice" of state.
  @select('isPinging') isPinging$: Observable<boolean>;

  constructor(
    private ngRedux: NgRedux<AppState>) {
    ngRedux.configureStore(
      pingReducer,
      INITIAL_STATE,
      middlewares,
      []);
    epicMiddleware.run(pingEpic);
  }

  public ping = () => this.ngRedux.dispatch(ping());
}

