import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgReduxModule } from '@angular-redux/store';
import { NgRedux, DevToolsExtension } from '@angular-redux/store';

import { rootReducer, IAppState } from './store/index';
import { EmployeesActions } from './actions/employees.actions'
import { AppComponent } from './app.component';

@NgModule({
  imports: [ NgReduxModule, 
             BrowserModule, 
             FormsModule, 
             HttpClientModule 
  ],
  declarations: [ AppComponent ],
  providers: [ EmployeesActions ],
  bootstrap: [ AppComponent ]
})
export class AppModule { 
  constructor(
    private ngRedux: NgRedux<IAppState>,
    private devTool: DevToolsExtension
  ) {

    this.ngRedux.configureStore(
      rootReducer,
      {} as IAppState,
      [ ],
      [ devTool.isEnabled() ? devTool.enhancer() : f => f]
    );

  }
}