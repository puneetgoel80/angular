export interface Employees {
  active: Employee;
  list: Employee[];
}

export interface Employee {
  // ? means optional.
  id?: number;
  first_name?: string;
  last_name?: string;
}
