import { Component, ViewChild } from '@angular/core';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs';

import { EmployeesActions } from './actions/employees.actions';
import { Employees } from './model/employees';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  @select('employees') public employees$: Observable<Employees>;
  @select(['employees', 'active']) active$;
  activeEmployee;

  constructor(public actions: EmployeesActions) {
    actions.getEmployees();
  }


  ngOnInit() {
    this.active$.subscribe(res => {
      this.activeEmployee = res;
    });
  }

  save(f: any) {
    // Merge form data with data model
    // (since form does not include all fields)
    const newEmployee = Object.assign({}, this.activeEmployee, f.value);
    this.actions.save(newEmployee);
  }

}
