export interface Employee {
    id: number,
    surname: string;
    salary: number;
}