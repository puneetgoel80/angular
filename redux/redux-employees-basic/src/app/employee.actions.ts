import {
  Action,
  ActionCreator
} from 'redux';
import { Employee } from './employee';

const EMPLOYEE_ADD: string = 'EMPLOYEE_ADD';
// (Q)
const EMPLOYEE_DELETE: string = 'EMPLOYEE_DELETE';

export class EmployeeAddAction implements Action {
  readonly type = EMPLOYEE_ADD;
  payload: Employee;
}

// (Q)
export class EmployeeDeleteAction implements Action {
  readonly type = EMPLOYEE_DELETE;
  payload: number;
}