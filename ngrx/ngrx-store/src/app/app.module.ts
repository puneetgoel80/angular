import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { AppComponent } from './app.component';
import { CounterDisplayComponent } from './counter-display.component';
// Q
import { fedoraCounterReducer, AppState, capCounterReducer, deerstalkerCounterReducer } from './state';

@NgModule({
  declarations: [
    AppComponent,
    CounterDisplayComponent
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot<AppState>({
      capCounter: capCounterReducer,
      fedoraCounter: fedoraCounterReducer,
      // Q
      deerstalkerCounter: deerstalkerCounterReducer
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 50
    }),
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
