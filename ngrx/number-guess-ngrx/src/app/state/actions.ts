import { Action } from '@ngrx/store';
// (Q) import Win
import { Win } from '../models/win';

export const GET_AVERAGE_GUESSES = 'Get Average Guesses';
export const GUESSES_RETRIEVED = 'Guesses Retrieved';

// (Q) Add constants.
export const REGISTER_WIN = 'Register Win';
export const REGISTER_WIN_COMPLETE = 'Register Win Complete';

export class GetAverageGuesses implements Action {
    readonly type = GET_AVERAGE_GUESSES;
}

export class GuessesRetrieved implements Action {
    readonly type = GUESSES_RETRIEVED;
    constructor(public payload: number) { }
}

// (Q) Add RegisterWin Action
export class RegisterWin implements Action {
    readonly type = REGISTER_WIN;
    constructor(public payload: Win) { }
}

// (Q) Add RegisterWinComplete Action.
export class RegisterWinComplete implements Action {
    readonly type = REGISTER_WIN_COMPLETE;
    constructor(public payload: Win) { }
}

// (Q) Add new Actions to be exported.
export type All =
    | GetAverageGuesses
    | GuessesRetrieved
    | RegisterWin
    | RegisterWinComplete;