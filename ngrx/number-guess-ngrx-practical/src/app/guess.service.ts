import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, delay } from 'rxjs/operators';
import { Win } from './models/win';

const API_URL = 'http://localhost:8080/number-guess-service/app/service';

@Injectable()
export class GuessService {

  private isRequesting: boolean = false;

  constructor(private http: HttpClient) { }

  getAverageGuesses(): Observable<number> {
    return this.http.get<number>(`${API_URL}/average`).pipe(
      delay(1000)
    );
  }

  registerWin(win: Win): Observable<any> {
    return this.http.post(API_URL, win).pipe(
      delay(1000),
      catchError((error: any) =>
        Observable.throw(error.json().error || 'Server error'))
    );
  }
}