import { Action } from '@ngrx/store';

export const GET_AVERAGE_GUESSES = 'Get Average Guesses';
export const GUESSES_RETRIEVED = 'Guesses Retrieved';

// (Q) Add REGISTER_WIN & REGISTER_WIN_COMPLETE constants.

export class GetAverageGuesses implements Action {
    readonly type = GET_AVERAGE_GUESSES;
}

export class GuessesRetrieved implements Action {
    readonly type = GUESSES_RETRIEVED;
    constructor(public payload: number) { }
}

// (Q) Add RegisterWin Action

// (Q) Add RegisterWinComplete Action.

// (Q) Add new Actions to be exported.
export type All =
    | GetAverageGuesses
    | GuessesRetrieved;