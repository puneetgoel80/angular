import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.css']
})
export class HelloComponent implements OnInit {

  message : string;
  constructor() {
    this.message = "what are we doing here";
   }

  ngOnInit() {
  }

}
