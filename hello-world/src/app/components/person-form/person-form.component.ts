import { Component, OnInit,EventEmitter, Output } from '@angular/core';


@Component({
  selector: 'app-person-form',
  templateUrl: './person-form.component.html',
  styleUrls: ['./person-form.component.css']
})
export class PersonFormComponent implements OnInit {

  
  answer : string = "";
  submitted : boolean = false;
  name : string = "";


  @Output() nameChangedEvent : EventEmitter<String> = new EventEmitter<String>();
  
  constructor() { }

  ngOnInit() {
  }

  onSubmit() : void {
    this.submitted = true;
    this.nameChangedEvent.emit(this.name);
  }

  

}
