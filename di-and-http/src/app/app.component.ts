import { Component } from '@angular/core';
import {HttpClient } from '@angular/common/http';

const API_BASE_URL = 'http://localhost:3000';

interface Employee {
  first_name :string;
  last_name : string;
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  employeeData : Employee[] = [];

  constructor(http:HttpClient){
  //http.get<Employee[]>(API_BASE_URL+'/employees');
    http
    .get<Employee[]>(`${API_BASE_URL}/employees`)
    .subscribe(employees => this.employeeData = employees);

  }



  title = 'di-and-http';
}
