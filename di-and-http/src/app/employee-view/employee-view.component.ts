import { Component, OnInit } from '@angular/core';
import { EmployeeService, Employee } from '../employee.service';

@Component({
  selector: 'app-employee-view',
  templateUrl: './employee-view.component.html',
  styleUrls: ['./employee-view.component.css']
})
export class EmployeeViewComponent implements OnInit {

  employeeData : Employee[] = [];
  
  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {
    this.employeeService.loadEmployees().subscribe(employees => this.employeeData =employees);


  }

  onSubmit(firstName:string,lastName:string) :void {
    
    this.employeeService.createEmployee({first_name : firstName,last_name:lastName})
    .subscribe(
        data=> console.log(data),
        err=> console.log(err),
        ()=> this.ngOnInit()
    );
  }


}
