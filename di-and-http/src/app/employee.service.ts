import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';


const API_BASE_URL = 'http://localhost:3000';

export interface Employee{
    id? : number;
    first_name?:string;
    last_name?:string;

}

@Injectable()
export class EmployeeService{

    constructor(private http:HttpClient){

    }

    loadEmployees() : Observable<Employee[]>{
        return this.http.get<Employee[]>(`${API_BASE_URL}/employees`);
    }

    createEmployee(data : Employee):Observable<number>{
        return this.http.post<number>(`${API_BASE_URL}/employees`,data);
    }

}