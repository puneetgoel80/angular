import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgReduxModule } from '@angular-redux/store';
import { NgRedux, DevToolsExtension } from '@angular-redux/store';

import { rootReducer, IAppState } from './store';
import { EmployeesActions } from './actions/employees.actions'
import { AppComponent } from './app.component';

import { createEpicMiddleware } from 'redux-observable';
import { EmployeesEpics } from './epics/employees.epics';

@NgModule({
  imports: [NgReduxModule, BrowserModule, FormsModule, HttpModule],
  declarations: [AppComponent],
  providers: [EmployeesActions, EmployeesEpics],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    private ngRedux: NgRedux<IAppState>,
    private epics: EmployeesEpics,
    private devTool: DevToolsExtension
  ) {

    const epicMiddleware = createEpicMiddleware();
    const middlewares = [epicMiddleware];

    this.ngRedux.configureStore(
      rootReducer,
      {} as IAppState,
      middlewares,
      [devTool.isEnabled() ? devTool.enhancer() : f => f]
    );
    epicMiddleware.run(epics.getEmployees);
  }
}
