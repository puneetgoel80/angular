export interface Employees {
  list: Employee[];
  errors: any
}

export interface Employee {
  // ? means optional.
  id?: number;
  first_name?: string;
  last_name?: string;
}
