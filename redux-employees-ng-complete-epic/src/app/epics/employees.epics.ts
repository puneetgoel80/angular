import { Injectable } from '@angular/core';
import { ActionsObservable, ofType } from 'redux-observable';
import { of } from 'rxjs';
import { map, catchError, mergeMap, debounceTime, takeUntil, delay } from 'rxjs/operators';
import { Http } from '@angular/http';
import { EmployeesActions } from '../actions/employees.actions';

// 2. Define constants.
const employeesGetSuccess = payload => ({
    type: EmployeesActions.EMPLOYEES_GET_SUCCESS,
    payload
})

@Injectable()
export class EmployeesEpics {

    API_URL = 'http://localhost:3000';

    constructor(private http: Http) {}

    getEmployees = (action$: ActionsObservable<any>) => {
        return action$.pipe(
            ofType<any>(EmployeesActions.EMPLOYEES_GET),
            mergeMap(() => {
                return this.http.get(`${this.API_URL}/employees`).pipe(
                    // 1. Do inline
                    // map(list => ({
                    //     type: EmployeesActions.EMPLOYEES_GET_SUCCESS,
                    //     payload: list.json()
                    // })),
                    // 2. Use constant.
                    delay(3000),
                    map(list => employeesGetSuccess(list.json())),
                    takeUntil(action$.ofType(EmployeesActions.EMPLOYEES_GET_CANCELLED)),
                    catchError(error => of({
                        type: EmployeesActions.EMPLOYEES_GET_ERROR,
                        error: true
                    }))
                );
            })
        );
    }

}
