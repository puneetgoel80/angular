import { InjectionToken } from '@angular/core';
import {  createStore, compose,  StoreEnhancer} from 'redux';

import * as Redux from 'redux';

import { AppState } from './app.state';
import {  counterReducer as reducer} from './counter.reducer';


export const AppStore = new InjectionToken('App.store');
const devtools : StoreEnhancer<AppState> = window['devToolsExtension'] ? window['devToolsExtension']() : f => f;


export function createAppStore():Redux.Store<AppState>{
    return createStore<AppState,any,any,any> (reducer, compose(devtools));
}

export const appStoreProvider = [{
provide:AppStore,useFactory : createAppStore
}];
