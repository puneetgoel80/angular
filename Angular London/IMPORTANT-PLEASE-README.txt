Hi,

Code and resources to accompany the Angular delivery.

If you have questions, or anything at all, please get in touch.

Cheers,

Greg

gc@gregcurtis.co.uk

NOTE: I cannot keep the share up indefinitely. It will be up for 7-10 days from today. Please download (NOT share to your own dropbox) as soon as possible. 

------------------
1st Recommendation
------------------

Manual has setup and intros to areas/code/setup.

Work through tutorial at https://angular.io

-------------
General Setup
-------------

Install Node, Angular CLI, VS Code. See tools/README.

Setup json-server. See README in /code/local-json-server/json-server-master.

Setup Tomcat. See tools/apache-tomcat-8.5.23.7z and tools/README.

Look for API URL's in code. Port 3000 indicates json-server, port 8080 indicates Tomcat.

----
Code
----

Split into appropriate directories. 

1. Unzip
2. Build using >npm install
3. Serve using >ng serve

-------------------
Resources Directory
-------------------

References and books/code for both Angular and JS.

Also highly recommend the You Don't Know JS series (ebooks are free):
 - https://github.com/getify/You-Dont-Know-JS

---------------
Links/Tools etc
---------------

General JS:
 - https://developer.mozilla.org/en-US/docs/Web/JavaScript

Augury Chrome extension: 
 - https://augury.angular.io/
 - https://theinfogrid.com/tech/developers/angular/augury-debug-profile-angular-app/

Redux devtools Chrome extension: 
 - https://github.com/zalmoxisus/redux-devtools-extension

Angular CLI
 - https://cli.angular.io/

Online VS Code
 - https://codesandbox.io/
 - https://stackblitz.com/ - search "stackblitz tour of heroes".

Online/shared JS:
 - https://jsfiddle.net/
 - https://jsbin.com
 - https://es6console.com
 - https://babeljs.io

TypeScript:
 - https://www.typescriptlang.org/index.html

Debugging Angular in VS Code
 - https://code.visualstudio.com/docs/nodejs/angular-tutorial

RxJS
 - http://reactivex.io/
 - https://rxjs-dev.firebaseapp.com/
 - https://angular.io/guide/rx-library