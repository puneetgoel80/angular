import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts';

import { AppComponent } from './app.component';
import { HashwordsComponent } from './hashwords/hashwords.component';
import { ChartsComponent } from './charts/charts.component';


@NgModule({
  declarations: [
    AppComponent,
    HashwordsComponent,
    ChartsComponent
  ],
  imports: [
    BrowserModule,
    ChartsModule
  ],
  providers: [
    { provide: 'Hashwords', useValue: window['hashwords']() }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
