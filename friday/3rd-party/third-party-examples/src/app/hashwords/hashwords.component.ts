import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'hashwords',
  templateUrl: './hashwords.component.html',
  styleUrls: ['./hashwords.component.css']
})
export class HashwordsComponent implements OnInit {

  constructor(@Inject('Hashwords') public hashwords: any) {
    const testHash = 'myemail@gmail.com';
    const humanHash = hashwords.hashStr(testHash);
    console.log('Human readbale hash :', humanHash);
  }

  ngOnInit() {
  }

}
