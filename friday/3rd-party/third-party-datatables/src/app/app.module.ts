import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { DataTablesModule } from 'angular-datatables';
import { ZeroConfigComponent } from './zero-config/zero-config.component';
import { ServerSideComponent } from './server-side/server-side.component';

@NgModule({
  declarations: [
    AppComponent,
    ZeroConfigComponent,
    ServerSideComponent
  ],
  imports: [
    BrowserModule,
    DataTablesModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
