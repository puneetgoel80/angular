import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { Store, StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { counter } from './counter.reducer';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot({ counter }),
    StoreDevtoolsModule.instrument({
      maxAge: 50
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
