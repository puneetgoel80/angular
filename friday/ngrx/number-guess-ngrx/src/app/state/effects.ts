import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { mergeMap, map } from 'rxjs/operators';
import { Effect, Actions } from '@ngrx/effects';

import { GuessService } from '../guess.service';
import * as GuessActions from './actions';
export type Action = GuessActions.All;

@Injectable()
export class Effects {
    private averageLoaded = false;

    constructor(
        private actions: Actions,
        private guessService: GuessService
    ) { }

    @Effect()
    retrieve: Observable<Action> = this.actions
        .ofType(GuessActions.GET_AVERAGE_GUESSES).pipe(
            mergeMap(() => this.guessService.getAverageGuesses()),
            map(average => {
                this.averageLoaded = true;
                return new GuessActions.GuessesRetrieved(average)
            })
        );

    @Effect()
    create: Observable<Action> = this.actions
        .ofType(GuessActions.REGISTER_WIN).pipe(
            map((action: GuessActions.RegisterWin) => action.payload),
            mergeMap(guess => this.guessService.registerWin(guess)),
            map(guess => new GuessActions.RegisterWinComplete(guess))
        );
}