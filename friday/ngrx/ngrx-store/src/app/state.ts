import { Action } from '@ngrx/store';

const ADD_CAP = 'ADD_CAP';
export class AddCapToCartAction implements Action {
  type = ADD_CAP;
}

const ADD_FEDORA = 'ADD_FEDORA';
export class AddFedoraToCartAction implements Action {
  type = ADD_FEDORA;
}

// Q
const ADD_DEERSTALKER = 'ADD_DEERSTALKER';
export class AddDeerstalkerToCartAction implements Action {
  type = ADD_DEERSTALKER;
}

const ADD_FEDORAS = 'ADD_FEDORAS';
export class AddFedorasToCartAction implements Action {
  type = ADD_FEDORAS;
  constructor(public payload: number) { }
}

const EMPTY_CART = 'EMPTY_CART';
export class EmptyCartAction implements Action {
  type = EMPTY_CART;
}

export interface AppState {
  capCounter: number;
  fedoraCounter: number;
  // Q
  deerstalkerCounter: number;
}

export function capCounterReducer
  (value: number = 0, action: Action): number {
    switch (action.type) {
      case ADD_CAP:
        return value + 1;

      case EMPTY_CART:
        return 0;

      default:
        return value;
    }
  }

export function fedoraCounterReducer(value: number = 0, action: Action): number {
  switch (action.type) {
    // If you have 10 x fedoras, you are limited to 10.
    case ADD_FEDORA:
      const x = value + 1;
      return x > 10 ? 10 : x;

    case ADD_FEDORAS:
      const y = value + (action as AddFedorasToCartAction).payload;
      return y > 10 ? 10 : y;

    case EMPTY_CART:
      return 0;

    default:
      return value;
  }
}

export function deerstalkerCounterReducer
  (value: number = 0, action: Action): number {
    switch (action.type) {
      case ADD_DEERSTALKER:
        return value + 1;

      case EMPTY_CART:
        return 0;

      default:
        return value;
    }
  }
