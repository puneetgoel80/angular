﻿import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// Q
import { AddCapToCartAction, 
         AppState,
         AddFedoraToCartAction, 
         AddFedorasToCartAction, 
         EmptyCartAction,
         AddDeerstalkerToCartAction } from './state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  cap: Observable<number>;
  fedora: Observable<number>;
  // Q
  deerstalker: Observable<number>;
  total: Observable<number>;

  constructor(public store: Store<AppState>) {
    this.cap = store.select(myAppState => myAppState.capCounter);
    this.fedora = store.select(state => state.fedoraCounter);
    // Q
    this.deerstalker = store.select(state => state.deerstalkerCounter);
    this.total = store.pipe(map(s => s.capCounter + s.fedoraCounter));
  }

  addCap() {
    this.store.dispatch(new AddCapToCartAction());
  }

  addFedora() {
    this.store.dispatch(new AddFedoraToCartAction());
  }

  addFedoras() {
    this.store.dispatch(new AddFedorasToCartAction(3));
  }

  // Q
  addDeerstalker() {
    this.store.dispatch(new AddDeerstalkerToCartAction());
  }

  empty() {
    this.store.dispatch(new EmptyCartAction());
  }
}
