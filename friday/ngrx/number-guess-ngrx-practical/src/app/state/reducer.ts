import { createSelector, createFeatureSelector } from '@ngrx/store';
import * as Actions from './actions';
import { Win } from '../models/win';

export interface State {
    win: Win
    average: number;
    loading: boolean;
}

const defaultState: State = {
    win: null,
    average: 0,
    loading: false
};

export function reducer(
    state: State = defaultState,
    action: Actions.All
): State {
    switch (action.type) {
        case Actions.GET_AVERAGE_GUESSES:
            return { ...state, loading: true };
        case Actions.GUESSES_RETRIEVED:
            return {
                ...state,
                average: action.payload,
                loading: false
            };
        // (Q) Add REGISTER_WIN and REGISTER_WIN_COMPLETE cases.
        default:
            return state;
    }
}

// 'app' is the name of the reducer at module level.
export const getState = createFeatureSelector<State>('app');

// createSelector performs memoization, keeping track of last input params
// to selector and if they are the same as current ones, returns last result 
// immediately instead of repeating computation.
export const getAverage = createSelector(
    getState,
    (state: State) => state.average
);

export const getLoading = createSelector(
    getState,
    (state: State) => state.loading
);

// Pre-5 way of doing things.
// export const getAverage = (state: State) => state.average;
// export const getLoading = (state: State) => state.loading;

// export function getState(store: any) {
//     let state: any;
//     store.take(1).subscribe(o => state = o);
//     return state;
// }

