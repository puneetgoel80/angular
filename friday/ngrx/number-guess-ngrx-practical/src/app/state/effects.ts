import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { mergeMap, map } from 'rxjs/operators';
import { Effect, Actions } from '@ngrx/effects';

import { GuessService } from '../guess.service';
import * as GuessActions from './actions';
export type Action = GuessActions.All;

@Injectable()
export class Effects {
    private averageLoaded = false;

    constructor(
        private actions: Actions,
        private guessService: GuessService
    ) { }

    @Effect()
    retrieve: Observable<Action> = this.actions
        .ofType(GuessActions.GET_AVERAGE_GUESSES).pipe(
            mergeMap(() => this.guessService.getAverageGuesses()),
            map(average => {
                this.averageLoaded = true;
                return new GuessActions.GuessesRetrieved(average)
            })
        );

    // (Q) Add create effect to deal with side-effect of calling service
    // and subsequently building a RegisterWinComplete action.

}