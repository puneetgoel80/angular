import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-page-two',
  templateUrl: './page-two.component.html',
  styleUrls: ['./page-two.component.css']
})
export class PageTwoComponent implements OnInit, OnDestroy {

  id: number;
  mySubscription: Subscription;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.mySubscription = this.route.params.subscribe(params => {
      this.id = +params['id'];  // (+) converts string 'id' to a number
      console.log(params)       // Log the entire params object
      console.log(params['id']) // Log the value of id
    });
  }

  ngOnDestroy() {
    this.mySubscription.unsubscribe();
  }

}
