import { Component, OnInit } from '@angular/core';
import { GuessService } from '../guess.service';
import { Win } from '../win';

@Component({
  selector: 'guess-using-service',
  templateUrl: './guess-using-service.component.html',
  styleUrls: ['./guess-using-service.component.css']
})
export class GuessUsingServiceComponent implements OnInit {

  private deviation: number;
  private guesses: number;
  private original: number;
  private guess: number;
  private showHigh: boolean = true;
  private showLow: boolean = true;
  private showWin: boolean = true;
  private average: number;
  private error: string;
 
  constructor(private service: GuessService) { }

  ngOnInit() {
    this.guesses = 0;
    this.original = Math.floor((Math.random() * 1000) + 1);
    console.log(this.original);
    this.guess = null;
    this.deviation = null;
    this.showHigh = true;
    this.showLow = true;
    this.showWin = true;
    this.service.getAverageGuesses().subscribe(
      average => this.average = average, 
      err => {
        alert("Could not fetch average.");
      });
  }

  verifyGuess() {
    this.deviation = this.original - this.guess;
    this.guesses = this.guesses + 1;

    this.showHigh = true;
    this.showLow = true;
    this.showWin = true;

    if(this.deviation < 0) {
      this.showHigh = false;
    } else if (this.deviation > 0) {
      this.showLow = false;
    } else {
      this.showWin = false;
      console.log("noOfTries: " + this.guesses);
      let win = new Win(this.guesses);
      console.log(win);
      this.service.registerWin(win).subscribe(
        result => console.log(result),
        error => this.error = <any>error);   
    }
  }

}
