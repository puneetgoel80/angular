import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'guess-original',
  templateUrl: './guess-original.component.html',
  styleUrls: ['./guess-original.component.css']
})
export class GuessOriginalComponent implements OnInit {

  private deviation: number;
  private guesses: number;
  private original: number;
  private guess: number;

  constructor() { }

  ngOnInit() {
    this.guesses = 0;
    this.original =  
       Math.floor((Math.random() * 1000) + 1);
    console.log(this.original);
    this.guess = null;
    this.deviation = null;
  }

  verifyGuess() {
    this.deviation = this.original - this.guess;
    this.guesses = this.guesses + 1;
  }

}
