import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'guess-refactored',
  templateUrl: './guess-refactored.component.html',
  styleUrls: ['./guess-refactored.component.css']
})
export class GuessRefactoredComponent implements OnInit {

  private deviation: number;
  private guesses: number;
  private original: number;
  private guess: number;
  private showHigh: boolean = true;
  private showLow: boolean = true;
  private showWin: boolean = true;
  
  constructor() { }

  ngOnInit() {
    this.guesses = 0;
    this.original = Math.floor((Math.random() * 1000) + 1);
    console.log(this.original);
    this.guess = null;
    this.deviation = null;
    this.showHigh = true;
    this.showLow = true;
    this.showWin = true;
  }

  verifyGuess() {
    this.deviation = this.original - this.guess;
    this.guesses = this.guesses + 1;

    this.showHigh = true;
    this.showLow = true;
    this.showWin = true;

    if(this.deviation < 0) {
      this.showHigh = false;
    } else if (this.deviation > 0) {
      this.showLow = false;
    } else {
      this.showWin = false;
    }
  }

}
