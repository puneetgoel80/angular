import { Injectable } from '@angular/core';

export class AuthService {
  loggedIn = false;

  isAuthenticated() {
    const promise = new Promise(
      (resolve, reject) => {
        setTimeout(() => {
          resolve(this.loggedIn);
        }, 1000);
      }
    );
    return promise;
  }

  login() {
    this.loggedIn = true;
    console.log(this.loggedIn);
  }

  logout() {
    this.loggedIn = false;
    console.log(this.loggedIn);
  }
}