import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SecuredComponent } from './secured/secured.component';
import { AuthGuardService } from './auth-guard.service';
import { AuthService } from './auth.service';

const myRoutes = [
  { path: '', component: HomeComponent },
  { path: 'secured', canActivate: [AuthGuardService], component: SecuredComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SecuredComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(myRoutes)
  ],
  providers: [AuthService, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
