import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-secured',
  templateUrl: './secured.component.html',
  styleUrls: ['./secured.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SecuredComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
