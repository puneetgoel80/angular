import {
  Action,
  ActionCreator
} from 'redux';
import { Employee } from './employee';

const EMPLOYEE_ADD: string = 'EMPLOYEE_ADD';
// (Q) Add EMPLOYEE_DELETE const

export class EmployeeAddAction implements Action {
  readonly type = EMPLOYEE_ADD;
  payload: Employee;
}

// (Q) Add EmployeeDeleteAction 