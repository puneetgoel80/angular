import { combineReducers } from 'redux';

import { EmployeesReducer } from './employees.reducer';
import { Employees } from '../model/employees';

export class IAppState {
  employees: Employees;
};

export const rootReducer = combineReducers<IAppState>({
  employees: EmployeesReducer,
});


