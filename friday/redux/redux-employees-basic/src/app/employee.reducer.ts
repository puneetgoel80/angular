import { Reducer, Action, ActionCreator } from 'redux';
import { AppState } from './app.state';
import {
  EmployeeAddAction,
  EmployeeDeleteAction
} from './employee.actions';
import { Employee } from './employee';

const initialState: AppState = {
  // employeeList: new Array(new Employee(1, "Curtis", 22222))
  // employeeList: [
  //   { "id": 1, surname: "King", salary: 22222 }
  // ]
  employeeList: []
};

export const employeeReducer: Reducer<AppState> =
  (state: AppState = initialState, action: any): AppState => {

    // (Q)
    let list;

    switch (action.type) {
      case 'EMPLOYEE_ADD':
        return Object.assign({}, state, {
          employeeList: state.employeeList.concat(action.payload)
        });

      // (Q)
      case 'EMPLOYEE_DELETE':
        list = state.employeeList
          .filter(({ id }) => id !== action.payload.id);
        return Object.assign({}, state, { employeeList: list });

      default:
        return state;
    }
  };