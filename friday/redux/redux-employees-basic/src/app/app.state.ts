import { Employee } from "./employee";

/*
 * Minimal app state
 * 
 * Here as the future state may become more complicated 
 * 
 */

export interface AppState {
  employeeList: Array<Employee>;
};

