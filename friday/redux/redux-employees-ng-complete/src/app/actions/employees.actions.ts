import { Injectable } from '@angular/core';
import { IAppState } from '../store/index';
import { NgRedux } from '@angular-redux/store';
import { Employee } from '../model/employees';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class EmployeesActions {
    static EMPLOYEES_GET = 'EMPLOYEES_GET';
    static EMPLOYEES_ADD = 'EMPLOYEES_ADD';
    static EMPLOYEES_UPDATE = 'EMPLOYEES_UPDATE';
    static EMPLOYEES_DELETE = 'EMPLOYEES_DELETE';
    static EMPLOYEES_GET_ACTIVE = 'EMPLOYEES_GET_ACTIVE';
    static EMPLOYEES_SET_ACTIVE = 'EMPLOYEES_SET_ACTIVE';
    static EMPLOYEES_RESET_ACTIVE = 'EMPLOYEES_RESET_ACTIVE';

    API_URL = 'http://localhost:3000';

    constructor(
        private ngRedux: NgRedux<IAppState>,
        private http: HttpClient
    ) {
    }

    getEmployees() {
        this.http.get(`${this.API_URL}/employees`)
            .subscribe((list) => {
                this.ngRedux.dispatch({
                    type: EmployeesActions.EMPLOYEES_GET,
                    payload: {
                        list
                    }
                });
                // Set first employee as active (dispatch action)
                this.setActiveEmployee(list[0].id);
            });
    }

    save(employee: Employee) {
        if (employee.id) {
            this.update(employee);
        } else {
            this.add(employee);
        }
    }

    add(employee: Employee): void {
        this.http.post(`${this.API_URL}/employees/`, employee)
            .subscribe((res: Employee) => {
                console.log(res);
                this.ngRedux.dispatch({
                    type: EmployeesActions.EMPLOYEES_ADD,
                    payload: { employee: res }
                });
                this.setActiveEmployee(employee.id);
            });
    }

    update(employee: Employee) {
        this.http.patch(`${this.API_URL}/employees/${employee.id}`, employee)
            .subscribe((res: Employee) => {
                this.ngRedux.dispatch({
                    type: EmployeesActions.EMPLOYEES_UPDATE,
                    payload: { employee: res }
                });
                this.setActiveEmployee(employee.id);
            });
    }

    deleteEmployee(id): void {
        this.http.delete(`${this.API_URL}/employees/${id}`)
            .subscribe((res) => {
                this.ngRedux.dispatch({
                    type: EmployeesActions.EMPLOYEES_DELETE,
                    payload: { id }
                });
                this.resetActive();
            });
    }

    setActiveEmployee(id: number): void {
        this.ngRedux.dispatch({
            type: EmployeesActions.EMPLOYEES_SET_ACTIVE,
            payload: { id }
        });
    }

    resetActive(): void {
        this.ngRedux.dispatch({
            type: EmployeesActions.EMPLOYEES_RESET_ACTIVE,
            payload: null
        });
    }
}
