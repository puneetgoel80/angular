import { EmployeesActions } from '../actions/employees.actions';
import { Employees } from '../model/employees';

const INITIAL_STATE: Employees = {
  list: [],
  active: {}
};

export function EmployeesReducer(state: Employees = INITIAL_STATE, action: any): any {
  let index, active, list;

  switch (action.type) {
    case EmployeesActions.EMPLOYEES_GET:
      return Object.assign({}, state, { list: action.payload.list });

    case EmployeesActions.EMPLOYEES_GET_ACTIVE:
      return state.active;

    case EmployeesActions.EMPLOYEES_DELETE:
      list = state.list
        .filter(({ id }) => id !== action.payload.id);
      return Object.assign({}, state, { list });

    case EmployeesActions.EMPLOYEES_ADD:
      state.list.push(action.payload.employee);
      return state;

    case EmployeesActions.EMPLOYEES_UPDATE:
      list = [...state.list];
      index = list.findIndex(({ id }) => id === action.payload.employee.id);
      list[index] = action.payload.employee;
      return Object.assign({}, state, { list });

    case EmployeesActions.EMPLOYEES_SET_ACTIVE:
      active = state.list.find(({id}) => id === action.payload.id);
      return Object.assign({}, state, { active });

    case EmployeesActions.EMPLOYEES_RESET_ACTIVE:
      return Object.assign({}, state, { active: {} });

    default:
      return state;
  }
}

