/*
 * minimal counter app state
 * 
 * In this case, our app state is simply a single number 
 * (the counter). But put it here as the future state may, 
 * become more complicated 
 * 
 */

export interface AppState {
  counter: number;
};

