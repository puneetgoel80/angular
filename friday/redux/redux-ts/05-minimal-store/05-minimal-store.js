class Store {
    constructor(reducer, initialState) {
        this.reducer = reducer;
        this._state = initialState;
    }
    getState() {
        return this._state;
    }
    dispatch(action) {
        this._state = this.reducer(this._state, action);
    }
}
let reducer = (state, action) => {
    switch (action.type) {
        case 'INCREMENT':
            return state + 1;
        case 'DECREMENT':
            return state - 1;
        case 'PLUS':
            return state + action.payload;
        default:
            return state;
    }
};
let store = new Store(reducer, 0);
console.log(store.getState());
store.dispatch({ type: 'INCREMENT' });
console.log(store.getState());
store.dispatch({ type: 'INCREMENT' });
console.log(store.getState());
store.dispatch({ type: 'DECREMENT' });
console.log(store.getState());
//# sourceMappingURL=05-minimal-store.js.map