"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const redux_1 = require("redux");
class MessageActions {
    static addMessage(message) {
        return {
            type: 'ADD_MESSAGE',
            message: message
        };
    }
    static deleteMessage(index) {
        return {
            type: 'DELETE_MESSAGE',
            index: index
        };
    }
}
let initialState = { messages: [] };
let reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_MESSAGE':
            return {
                messages: state.messages.concat(action.message),
            };
        case 'DELETE_MESSAGE':
            let idx = action.index;
            return {
                messages: [
                    ...state.messages.slice(0, idx),
                    ...state.messages.slice(idx + 1, state.messages.length)
                ]
            };
        default:
            return state;
    }
};
let store = redux_1.createStore(reducer);
console.log(store.getState());
store.dispatch(MessageActions.addMessage('Would you say the fringe was made of silk?'));
store.dispatch(MessageActions.addMessage('Wouldnt have no other kind but silk'));
store.dispatch(MessageActions.addMessage('Has it really got a team of snow white horses?'));
console.log(store.getState());
store.dispatch(MessageActions.deleteMessage(1));
console.log(store.getState());
store.dispatch(MessageActions.deleteMessage(0));
console.log(store.getState());
//# sourceMappingURL=09-real-redux.js.map