let reducer = (state, action) => {
    switch (action.type) {
        case 'INCREMENT':
            return state + 1;
        case 'DECREMENT':
            return state - 1;
        default:
            return state;
    }
};
let incrementAction = { type: 'INCREMENT' };
console.log(reducer(0, incrementAction));
console.log(reducer(1, incrementAction));
let decrementAction = { type: 'DECREMENT' };
console.log(reducer(100, decrementAction));
let unknownAction = { type: 'UNKNOWN' };
console.log(reducer(100, unknownAction));
//# sourceMappingURL=03-adjusting-reducer-switch.js.map