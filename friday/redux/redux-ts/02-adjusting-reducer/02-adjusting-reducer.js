let reducer = (state, action) => {
    if (action.type === 'INCREMENT') {
        return state + 1;
    }
    if (action.type === 'DECREMENT') {
        return state - 1;
    }
    return state;
};
let incrementAction = { type: 'INCREMENT' };
console.log(reducer(0, incrementAction));
console.log(reducer(1, incrementAction));
let decrementAction = { type: 'DECREMENT' };
console.log(reducer(100, decrementAction));
//# sourceMappingURL=02-adjusting-reducer.js.map