import { EmployeesActions } from '../actions/employees.actions';
import { Employee } from '../model/employees';

const INITIAL_STATE: Employee[] = [];

const FAKE_DATA: Employee[] = [
  { id: 1, first_name: "Greg", last_name: "Curtis" },
  { id: 2, first_name: "Bob", last_name: "Bobson" },
  { id: 3, first_name: "Larry", last_name: "Large" },
]

export function EmployeesReducer(state: Employee[] = INITIAL_STATE, action: any): any {
  switch (action.type) {
    case EmployeesActions.EMPLOYEES_GET:
      return [...FAKE_DATA];

    case EmployeesActions.EMPLOYEES_DELETE:
      return state.filter(({ id }) => id !== action.payload.id);

    default:
      return state;
  }
}

