import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-component',
  templateUrl: './my-component.component.html',
  styleUrls: ['./my-component.component.css']
})
export class MyComponentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  title = 'E2E Testing';
  score = 0;

  increment() {
    this.score++;
  }

  reset() {
    this.score = 0;
  }

}
