import { MyComponentPage } from './app.po';

describe('ex-e2e App', () => {
  let page: MyComponentPage;

  beforeEach(() => {
    page = new MyComponentPage();
  });

  it('Should display E2E Testing title', () => {
    page.navigateTo();
    expect(page.getTitle()).toEqual('E2E Testing');
  });

  it('Should start with score of 0', () => {
    page.navigateTo();
    expect(page.getPoints()).toEqual('0');
  });

  it('Should increase score by clicking increment button', () => {
    page.navigateTo();

    expect(page.getPoints()).toEqual('0');
    page.getIncrementButton().click();

    expect(page.getPoints()).toEqual('1');

    page.getIncrementButton().click();
    page.getIncrementButton().click();
    page.getIncrementButton().click();

    expect(page.getPoints()).toEqual('4');
  });

  it('Should reset score by clicking reset', () => {
    page.navigateTo();

    page.getIncrementButton().click();
    page.getIncrementButton().click();
    page.getIncrementButton().click();

    expect(page.getPoints()).toEqual('3');

    page.getResetButton().click();

    expect(page.getPoints()).toEqual('0');
  });
});
