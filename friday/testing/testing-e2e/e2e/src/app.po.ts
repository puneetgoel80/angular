import { browser, by, element } from 'protractor';

export class MyComponentPage {
  navigateTo() {
    return browser.get('/');
  }

  getTitle() {
    return element(by.css('h1')).getText();
  }

  getPoints() {
    return element(by.cssContainingText('div', 'Score')).$('span').getText();
  }

  getIncrementButton() {
    return element(by.cssContainingText('button', '+1'));
  }

  getResetButton() {
    return element(by.cssContainingText('button', 'Reset'));
  }
}
