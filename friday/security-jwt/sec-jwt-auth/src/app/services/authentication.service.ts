﻿import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// Real
const API_AUTH_URL = 'http://localhost:8080/sec-jwt-auth/authentication';
// Mock
// const API_AUTH_URL = '/api/authenticate';

@Injectable()
export class AuthenticationService {
    public token: string;

    constructor(private http: Http) {
        // set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
    }

    // Login using /sec-jwt-auth (comment out fakeBackendProvider, MockBackend & BaseRequestOptions) in module.
    login(username: string, password: string): Observable<boolean> {

        const body = new HttpParams()
            .set(`username`, username)
            .set(`password`, password);
        const headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        const options = new RequestOptions({ headers: headers });

        return this.http.post(API_AUTH_URL, body.toString(), options)
            .pipe(
                map((response: Response) => {
                    // login successful if there's a jwt token in the response
                    let token = response.json() && response.json().authToken;
  
                    if (token) {
                        // set token property
                        this.token = token;

                        // store username and jwt token in local storage to keep user logged in between page refreshes
                        localStorage.setItem('currentUser', JSON.stringify({ username: username, token: token }));

                        // return true to indicate successful login
                        return true;
                    } else {
                        // return false to indicate failed login
                        return false;
                    }
                })
            );
    }

    // Login using fakeBackend.
    // login(username: string, password: string): Observable<boolean> {

    //     return this.http.post(API_AUTH_URL, JSON.stringify({ username: username, password: password }))
    //         .pipe(
    //             map((response: Response) => {
    //                 // login successful if there's a jwt token in the response
    //                 let token = response.json() && response.json().token;
    //                 if (token) {
    //                     // set token property
    //                     this.token = token;

    //                     // store username and jwt token in local storage to keep user logged in between page refreshes
    //                     localStorage.setItem('currentUser', JSON.stringify({ username: username, token: token }));

    //                     // return true to indicate successful login
    //                     return true;
    //                 } else {
    //                     // return false to indicate failed login
    //                     return false;
    //                 }
    //             })
    //         );
    // }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');
    }
}