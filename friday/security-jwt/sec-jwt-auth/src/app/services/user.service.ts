﻿import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AuthenticationService } from './authentication.service';
import { User } from '../models/index';

// Real
const API_USERS_URL = 'http://localhost:8080/sec-jwt-auth/users';
// Mock
// const API_AUTH_URL = '/api/users';

@Injectable()
export class UserService {
    constructor(
        private http: Http,
        private authenticationService: AuthenticationService) {
    }

    getUsers(): Observable<User[]> {
        // add authorization header with jwt token
        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token, 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        console.log(options)

        // get users from api
        return this.http.get(API_USERS_URL, options)
            .pipe(
                map((response: Response) => response.json())
            );
    }
}