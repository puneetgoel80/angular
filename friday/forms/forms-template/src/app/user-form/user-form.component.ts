import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { User }    from '../user';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class UserFormComponent implements OnInit {

  user = new User(111, 'msmith', 'ms@mikesmith.co.uk');
  submitted = false;
  questions = ['Favourite football team?', 'Name of first pet?', 'City where you were born?'];
  defaultQuestion = 'Favourite football team?';
  answer = '';

  constructor() { }

  ngOnInit() {
  }
  
  onSubmit() { 
    this.submitted = true;
    console.log(this.submitted);
    console.log(this.user.username);
  }

  get diagnostic() { 
    return JSON.stringify(this.user); 
  }
}
