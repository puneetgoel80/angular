import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder }
    from '@angular/forms';

@Component({
    selector: 'app-user-form',
    templateUrl: './user-form.component.html',
    styleUrls: ['./user-form.component.css'],
})
export class UserForm {
    form: FormGroup;

    hasSpecialChars = (input: FormControl) => {
        const hasExclam = input.value.includes('!');
        const hasHash = input.value.includes('#');
        if (hasExclam || hasHash) {
            return { validPass: true }
        } else {
            return null;
        }
    }

    username = new FormControl("", Validators.required);
    password = new FormControl("", [
        Validators.required,
        Validators.minLength(8),
        this.hasSpecialChars
    ]);

    constructor(fb: FormBuilder) {
        this.form = fb.group({
            "username": this.username,
            "password": this.password
        });
    }
    onSubmit() {
        console.log(this.password.valid);
    }
}