import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from "@angular/forms";
import { UserForm } from './components/user-form.component';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    UserForm
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
