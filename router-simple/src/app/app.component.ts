import { Component } from '@angular/core';
// 4
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // 5
  constructor(private router: Router) {

  }
}
