package com.training;

import java.util.List;

public interface EmployeeDao {

	public abstract List<Employee> findAll();

	public abstract List<Employee> findByName(String name);
	
	public abstract Employee findByUsername(String name);

	public abstract Employee findById(int id);

	public abstract Employee save(Employee person);

	public abstract Employee create(Employee person);

	public abstract Employee update(Employee person);

	public abstract boolean remove(int id);

}