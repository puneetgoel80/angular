package com.training;

public interface ForecastDao {

	Forecast getForecast();

	Forecast getForecast(int from, int to);
}
