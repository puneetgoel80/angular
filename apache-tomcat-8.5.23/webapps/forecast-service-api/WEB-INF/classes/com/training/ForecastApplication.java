package com.training;

import java.util.Set;
import java.util.HashSet;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/sunday")
public class ForecastApplication extends Application {
    @Override
    public Set<Class<?>> getClasses() {
    	Set<Class<?>> set = new HashSet<Class<?>>();
        set.add(ForecastService.class);
        return set;
    }
}