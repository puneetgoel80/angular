package com.training;

import java.util.Random;

public class ForecastDaoImpl implements ForecastDao {

	@Override
	public Forecast getForecast() {
		String outlook = "poor";
		if(generateNoBetween10And100() >= 60) {
			outlook = "good";
		}
		return new Forecast(outlook);
	}

	private int generateNoBetween10And100() {
		Random r = new Random();
		int low = 10;
		int high = 100;
		return r.nextInt(high - low) + low;
	}

	@Override
	public Forecast getForecast(int from, int to) {
		return null;
	}

}
